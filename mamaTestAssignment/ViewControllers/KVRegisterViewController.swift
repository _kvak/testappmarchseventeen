//
//  KVRegisterViewController.swift
//  mamaTestAssignment
//
//  Created by Andre Kvashuk on 3/1/17.
//  Copyright © 2017 Andre Kvashuk. All rights reserved.
//

import UIKit
import Foundation

class KVRegisterViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var middleContainerView: UIView!
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var loginBtn: UIButton!
    //labels
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    //icons
    @IBOutlet weak var mailImageView: UIImageView!
    @IBOutlet weak var lockImageView: UIImageView!
    
    //MARK: Constraints for keyboard handling
    var isLabelSlided = false
    var isLoginBtnPushed = false
    
    @IBOutlet weak var middleContainerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginBtnBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordLblCenterXConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailLblCenterX: NSLayoutConstraint!
    
    var initialMidContainerBtnBottomConstraintValue:CGFloat?
    
    //common value for left slide of labels
    lazy var labelSlideValue:CGFloat = {
        let padding:CGFloat = 32.0
        let maxX: CGFloat
        if (self.mailImageView.frame.maxX > self.lockImageView.frame.maxX) {
            maxX = self.mailImageView.frame.maxX
        } else {
            maxX = self.lockImageView.frame.maxX
        }
        
        let difference = self.emailLbl.frame.origin.x - maxX - padding
        
        return difference
    }()
    

    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showProfileIfLoggedIn()
        
        setupViews()
        setupGestures()
        subscribeForNotifications()
        initialMidContainerBtnBottomConstraintValue = middleContainerViewBottomConstraint.constant        
    }

    //MARK: - Preparations
    
    func setupViews() {
        loginBtn.layer.cornerRadius = loginBtn.frame.height / 2
        loginBtn.isEnabled = false
    }
    
    func setupGestures() {
        let emailLblTap = UITapGestureRecognizer(target: self, action: #selector(handleEmailLblTap))
        emailLbl.addGestureRecognizer(emailLblTap)
        emailLbl.isUserInteractionEnabled = true
        
        let passwordLblTap = UITapGestureRecognizer(target: self, action: #selector(handlePasswordLblTap))
        passwordLbl.addGestureRecognizer(passwordLblTap)
        passwordLbl.isUserInteractionEnabled = true
    }

    func subscribeForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillAppear(notification:)), name:Notification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardWillDissappear(notification:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleTextFieldDidChange(notification:)), name: Notification.Name.UITextFieldTextDidChange, object:nil)
    }
    
    
    func validateTextFields() -> Bool {
        return KVValidator.sharedInstance.validateEmail(email: emailTF.text!) && KVValidator.sharedInstance.validatePassword(password: passwordTF.text!)
    }
    
    //MARK: - Actions
    
    func handleEmailLblTap() {
        emailTF.becomeFirstResponder()
    }
    
    func handlePasswordLblTap() {
        passwordTF.becomeFirstResponder()
    }
    
    @IBAction func handleLoginBtn(_ sender: UIButton) {
        let dict = makeUserDict()
        DispatchQueue.global(qos: .background).sync {
            self.fileHandler.saveUserDict(dict: dict)
            
            DispatchQueue.global(qos: .userInteractive).sync {
                showProfileController()
            }
        }
    }
    
    func makeUserDict() -> [String: String] {
        var userDict = [String: String]()
        userDict["email"] = emailTF.text
        userDict["password"] = passwordTF.text
        
        return userDict
    }
    
    func showProfileIfLoggedIn() {
            if let userDict = fileHandler.getUserDict() {
                let currentEmail = userDict["email"] as! String
                let currentPassword = userDict["password"] as! String
                
                let bothValid = KVValidator.sharedInstance.validateEmail(email: currentEmail) && KVValidator.sharedInstance.validatePassword(password: currentPassword)
                
                if bothValid {
                    DispatchQueue.global(qos: .userInteractive).async {
                        self.showProfileController()
                    }
                }
            }
    }
    //MARK: - Keyboard handling
    
    func handleKeyboardWillAppear(notification: NSNotification) {
        
        if isLoginBtnPushed {
            return
        }
        
        let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! CGRect
        
        let topButtonPadding:CGFloat = 30
        let newPaddedLoginBtnOrigin = loginBtn.frame.origin.y + topButtonPadding - keyboardFrame.height;
        var differenceForMiddleContainer:CGFloat = 0
        
        let isButtonHittingContainerView = newPaddedLoginBtnOrigin < middleContainerView.frame.maxY
        
        if  isButtonHittingContainerView {
            differenceForMiddleContainer = middleContainerView.frame.maxY - newPaddedLoginBtnOrigin
        }
        
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseInOut, animations: {
            self.loginBtnBottomConstraint.constant += keyboardFrame.height
            self.middleContainerViewBottomConstraint.constant -= differenceForMiddleContainer
            
            self.handleLabelsOnKeyboardAppearence()
            self.isLoginBtnPushed = true
        }, completion: nil)
    }
    
    func handleKeyboardWillDissappear(notification: NSNotification) {
        let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! CGRect

        UIView.animate(withDuration: 0.3) {
            self.loginBtnBottomConstraint.constant -= keyboardFrame.height
            self.middleContainerViewBottomConstraint.constant = self.initialMidContainerBtnBottomConstraintValue!
            self.handleLabelsOnKeyboardHide()
            self.isLoginBtnPushed = false
        }
    }
    
    //MARK: - Labels handling
    func handleLabelsOnKeyboardAppearence() {
        if isLabelSlided {
            return
        }
        
        emailLblCenterX.constant -= labelSlideValue
        emailLbl.textAlignment = .left
        emailLbl.text = emailLbl.text?.appending(":")
        
        passwordLblCenterXConstraint.constant -= labelSlideValue
        passwordLbl.textAlignment = .left
        passwordLbl.text = passwordLbl.text?.appending(":")
        
        self.isLabelSlided = true
    }
    
    func handleLabelsOnKeyboardHide() {
        if textFieldsNotEmpty() {
            return
        }
        
        self.emailLblCenterX.constant = 0
        self.emailLbl.textAlignment = .center
        self.emailLbl.text = self.emailLbl.text?.replacingOccurrences(of: ":", with: "")
        
        self.passwordLblCenterXConstraint.constant = 0
        self.passwordLbl.textAlignment = .center
        self.passwordLbl.text = self.passwordLbl.text?.replacingOccurrences(of: ":", with: "")
        
        self.isLabelSlided = false
    }
    
    func textFieldsNotEmpty() -> Bool {
        return emailTF.text!.characters.count > 0 || passwordTF.text!.characters.count > 0
    }
    
    //MARK: - TextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func handleTextFieldDidChange(notification:Notification) {
        
        let textField = notification.object as? UITextField
        
        if textField != emailTF && textField != passwordTF {
            return
        }
        
        if validateTextFields() {
            enableLoginBtn()
        } else {
            disableLoginBtn()
        }
    }

    //MARK: - Login button switching
    func enableLoginBtn() {
        loginBtn.isEnabled = true
        loginBtn.backgroundColor = UIColor(red: 10/255.0, green: 184/255.0, blue: 180/255.0, alpha: 1)
    }
    
    func disableLoginBtn() {
        loginBtn.isEnabled = false
        loginBtn.backgroundColor = UIColor(red: 10/255.0, green: 184/255.0, blue: 180/255.0, alpha: 0.5)
    }
    
    func showProfileController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier :"KVProfileViewController")
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
}
