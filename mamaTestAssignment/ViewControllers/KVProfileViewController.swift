//
//  KVProfileViewController.swift
//  mamaTestAssignment
//
//  Created by Andre Kvashuk on 3/4/17.
//  Copyright © 2017 Andre Kvashuk. All rights reserved.
//

import UIKit

class KVProfileViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    
    var textFields:[UITextField]!
    var tempDict:[String: String]!
    
    var editingMode:Bool! {
        didSet {
            if editingMode == true {
                self.rightBarImageView.image = UIImage(named: "tick_icon")
                self.leftBarImageView.image = UIImage(named: "close_icon")
                self.startEditing()
            } else {
                self.rightBarImageView.image = UIImage(named: "edit_icon")
                self.leftBarImageView.image = UIImage(named: "back_icon")
                self.stopEditing()
            }
        }
    }
    
    @IBOutlet weak var leftBarImageView: UIImageView!
    @IBOutlet weak var rightBarImageView: UIImageView!
    
    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "User Info"
        
        textFields = [emailTF, passwordTF, firstNameTF, lastNameTF]
        setupContent()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        editingMode = false
    }
    
    //MARK: - Preparations
    func setupContent() {
        let userDict = fileHandler.getUserDict()
        fillUpTextFieldsWith(dict: userDict as! [String : String])
    }
    
    func makeUserDict() -> [String: String] {
        var userDict = [String: String]()
        
        userDict["email"] = emailTF.text
        userDict["password"] = passwordTF.text
        userDict["firstName"] = firstNameTF.text
        userDict["lastName"] = lastNameTF.text
        
        return userDict
    }
    
    func fillUpTextFieldsWith(dict:[String: String]) {
        emailTF.text = dict["email"]
        passwordTF.text = dict["password"]
        firstNameTF.text = dict["firstName"]
        lastNameTF.text = dict["lastName"]
    }
    
    //MARK: - Actions
    
    @IBAction func handleRightBarButton(_ sender: Any) {
        if editingMode == true {
            saveChanges()
        }
        editingMode = !editingMode
    }
    
    @IBAction func handleLeftBarButton(_ sender: Any) {
        if editingMode == false {
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            discardChanges()
            editingMode = false
        }
    }

    //MARK: - TextFields handling
    
    func startEditing() {
        tempDict = makeUserDict()
        self.enableTextFields(enabled: true)
    }
    
    func stopEditing() {
        self.enableTextFields(enabled: false)
    }
    
    func enableTextFields(enabled: Bool) {
        for textField in textFields {
            textField.isUserInteractionEnabled = enabled
            textField.textColor = enabled ? .black : .gray
        }
    }
    
    //MARK: -
    func saveChanges() {
        let userDict = makeUserDict()
        DispatchQueue.global(qos: .background).async {
            self.fileHandler.saveUserDict(dict: userDict)
        }
    }
    
    func discardChanges() {
        fillUpTextFieldsWith(dict: tempDict)
    }

}
