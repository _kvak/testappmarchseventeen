//
//  BaseViewController.swift
//  mamaTestAssignment
//
//  Created by Andre Kvashuk on 3/4/17.
//  Copyright © 2017 Andre Kvashuk. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    var fileHandler = KVFileHandler()

    override var prefersStatusBarHidden : Bool {
        return true
    }

}
