//
//  KVValidator.swift
//  mamaTestAssignment
//
//  Created by Andre Kvashuk on 3/2/17.
//  Copyright © 2017 Andre Kvashuk. All rights reserved.
//

import UIKit

class KVValidator: NSObject {
    static var sharedInstance = KVValidator()
    
    public func validateEmail(email: String) -> Bool {
        let regex = "[A-Z0-9a-z._%+-]{3}+@[A-Za-z0-9.-]{3}+\\.[A-Za-z]{2}";
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        
        return predicate.evaluate(with: email)
    }
    
    public func validatePassword(password: String) -> Bool {
        let minPasswordLength = 5
        
        return password.characters.count >= minPasswordLength
    }

}
