//
//  KVFileHandler.swift
//  mamaTestAssignment
//
//  Created by Andre Kvashuk on 3/4/17.
//  Copyright © 2017 Andre Kvashuk. All rights reserved.
//

import UIKit

class KVFileHandler: NSObject {
    let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let userFileName = "userDict.txt"
    
    func archiveUrl() -> URL {
        print(">>>archiveUrl = \(documentsDirectory.appendingPathComponent(userFileName))")
        return documentsDirectory.appendingPathComponent(userFileName)
    }

    func readDictFrom(url:URL) -> [String: Any]? {
        do {
            let data = try Data(contentsOf: url)
            let dict = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any]
            print(">>> readDictFrom = \(dict)")
            return dict
        } catch let error {
            print(">>> json parsing error =\(error)")
            return nil
        }
    }
    
    public func getUserDict() -> [String: Any]? {
        return readDictFrom(url: self.archiveUrl())
    }

    public func saveUserDict(dict: [String: Any]) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            try jsonData.write(to: archiveUrl())
        } catch let error {
            print("\(error)")
        }
    }

}
